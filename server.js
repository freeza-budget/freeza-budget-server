var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;

var bodyParser = require('body-parser')
    app.use( bodyParser.json() );       // to support JSON-encoded bodies
    app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
        extended: true
    }));
    const fs = require("fs");
    const uuidv4 = require('uuid/v4');

// Read data from bill
app.route('/bill')
    .post(function (req, res) {
        var imgBase64 = req.body.image;
        var date = new Date();

        var filePath = "img/bill_image_" + date.getFullYear() + date.getMonth() + date.getDay() + date.getHours()
            + date.getMinutes() + date.getSeconds() + ".png";

        fs.writeFile(filePath, imgBase64, 'base64', function(err) {
            if(err) {
                console.log("Something wrong! Error: " + err);
            }
        });

        // command bus
        res.status(200).json({
             "id": uuidv4()
        });
    });

// Create new bill in database
app.route('/bill/:id')
    .post(function (req, res) {
        var billId = req.params.id,
            shopName = req.body.shop_name,
            long = req.body.long,
            lat = req.body.lat,
            products = req.body.products;

        res.status(200).json({
            "id": billId,
            "status": "OK"
        });
    })
    // Get products for bill
    .get(function(req, res) {
        var billId = req.params.id;

        res.status(200).json({
            "shop_name": "Biedronka",
            "long": 50.051801,
            "lat": 19.8740202,
            "products": [
                {
                    "name": "Mazak",
                    "price": 15.23,
                    "category": "Office supplies",
                    "amount": 3
                },
                {
                    "name": "Filiżanka",
                    "price": 48.50,
                    "category": "Home accesories",
                    "amount": 10
                },
                {
                    "name": "Zeszyt",
                    "price": 22.40,
                    "category": "Office supplies",
                    "amount": 1
                },
                {
                    "name": "Wino",
                    "price": 43.00,
                    "category": "Food",
                    "amount": 2
                }
            ]
        });
    });

app.route('/reports')
    .get(function(req, res) {

        res.status(200).json({
            "sum": "45.0",
            "categories": [
                {
                    "name": "Food",
                    "price": 500,
                    "percent": 32
                },
                {
                    "name": "Home",
                    "price": 700,
                    "percent": 42
                },
                {
                    "name": "Entertaiment",
                    "price": 200,
                    "percent": 26
                }
            ],
            "products": [
                {
                    "name": "Mazak",
                    "price": 15.23,
                    "category": "Office supplies",
                    "amount": 3
                },
                {
                    "name": "Filiżanka",
                    "price": 48.50,
                    "category": "Home accesories",
                    "amount": 10
                },
                {
                    "name": "Zeszyt",
                    "price": 22.40,
                    "category": "Office supplies",
                    "amount": 1
                },
                {
                    "name": "Wino",
                    "price": 43.00,
                    "category": "Food",
                    "amount": 2
                }
            ]
        });
    });

app.listen(port);
console.log('todo list RESTful API server started on: ' + port);